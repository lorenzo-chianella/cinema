package it.pegaso.cinemagit.business;

import java.io.IOException;
import java.sql.Time;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.pegaso.cinemagit.dao.FilmDao;
import it.pegaso.cinemagit.model.Film;

/**
 * Servlet implementation class PostFilm
 */
public class PostFilm extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PostFilm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FilmDao dao = new FilmDao();
		String titolo = request.getParameter("titolo");
		Film film = dao.getByTitolo(titolo);
		
		if(film == null) {
			Film f = new Film();
			f.setTitolo(request.getParameter("titolo"));
			f.setRegista(request.getParameter("regista"));
			f.setGenere(request.getParameter("genere"));
			f.setnPosti(Integer.parseInt(request.getParameter("posti")));
			f.setOraInizio(Time.valueOf(request.getParameter("oraInizio")));
			f.setDescrizione(request.getParameter("descrizione"));
			dao.insert(f);
			
		} else {
			Film f = dao.getByTitolo(titolo);
			f.setTitolo(request.getParameter("titolo"));
			f.setRegista(request.getParameter("regista"));
			f.setGenere(request.getParameter("genere"));
			f.setnPosti(Integer.parseInt(request.getParameter("posti")));
			f.setOraInizio(Time.valueOf(request.getParameter("oraInizio")));
			f.setDescrizione(request.getParameter("descrizione"));
			dao.update(f);
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("FilmSalvato.jsp");
		rd.forward(request, response);
	}

}
