package it.pegaso.cinemagit.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.pegaso.cinemagit.model.Film;

public class FilmDao {

	private final static String CONNECTION_STRING = "jdbc:mysql://localhost:3306/cinema?user=root&password=root&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	private Connection connection;
	private PreparedStatement getByTitolo;
	private PreparedStatement insert;
	private PreparedStatement update;
	
	public void insert(Film film) {
		try {
			getInsert().clearParameters();
			getInsert().setString(1, film.getTitolo());
			getInsert().setString(2, film.getRegista());
			getInsert().setString(3, film.getGenere());
			getInsert().setInt(4, film.getnPosti());
			getInsert().setTime(5, film.getOraInizio());
			getInsert().setString(6, film.getDescrizione());
			getInsert().execute();
			
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Errore durante l'INSERIMENTO!");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}		
	}
	
	public void update(Film film) {
		try {
			getUpdate().clearParameters();
			getUpdate().setString(1, film.getTitolo());
			getUpdate().setString(2, film.getRegista());
			getUpdate().setString(3, film.getGenere());
			getUpdate().setInt(4, film.getnPosti());
			getUpdate().setTime(5, film.getOraInizio());
			getUpdate().setString(6, film.getDescrizione());
			getUpdate().setLong(7, film.getId());
			getUpdate().execute();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Errore durante l'UPDATE!");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<Film> getAll(){
		List<Film> list = new ArrayList<Film>();
		
		try {
			ResultSet rs = getConnection().createStatement().executeQuery("SELECT * FROM film");
			while(rs.next()) {
				Film f = new Film();
				f.setId(rs.getLong("id"));
				f.setTitolo(rs.getString("titolo"));
				f.setRegista(rs.getString("regista"));
				f.setGenere(rs.getString("genere"));
				f.setnPosti(rs.getInt("posti"));
				f.setOraInizio(rs.getTime("oraInizio"));
				f.setDescrizione(rs.getString("descrizione"));
				
				list.add(f);
				
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	
	public Film getByTitolo(String titolo) {
		Film f = null;
		try {
			getGetByTitolo().clearParameters();
			getGetByTitolo().setString(1, titolo);
			ResultSet rs = getGetByTitolo().executeQuery();
			
			if(rs.next()) {
				f = new Film();
				f.setId(rs.getLong("id"));
				f.setTitolo(rs.getString("titolo"));
				f.setGenere(rs.getString("genere"));
				f.setRegista(rs.getString("regista"));
				f.setDescrizione(rs.getString("descrizione"));
				f.setnPosti(rs.getInt("posti"));
				f.setOraInizio(rs.getTime("oraInizio"));			
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return f;
	}
	
	public Connection getConnection() throws SQLException, ClassNotFoundException {
		if(connection == null) {
			connection = DriverManager.getConnection(CONNECTION_STRING);
		}
		return connection;
	}
	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public PreparedStatement getGetByTitolo() throws ClassNotFoundException, SQLException {
		if(getByTitolo==null) {
			getByTitolo = getConnection().prepareStatement("SELECT * FROM film WHERE titolo = ?");
		}
		return getByTitolo;
	}

	public void setGetByTitolo(PreparedStatement getByTitolo) {
		this.getByTitolo = getByTitolo;
	}
	
	public PreparedStatement getInsert() throws SQLException, ClassNotFoundException{
		if(insert == null) {
			insert = getConnection().prepareStatement("INSERT INTO film (titolo, regista, genere, posti, oraInizio, descrizione) VALUES (?, ?, ?, ?, ?, ?)");
		}
		return insert;
	}

	public void setInsert(PreparedStatement insert) {
		this.insert = insert;
	}

	public PreparedStatement getUpdate() throws ClassNotFoundException, SQLException {
		if(update == null) {
			update = getConnection().prepareStatement("UPDATE film SET titolo = ?, regista = ?, genere = ?, posti = ?, oraInizio = ?, descrizione = ? WHERE id = ?");
		}
		return update;
	}

	public void setUpdate(PreparedStatement update) {
		this.update = update;
	}

	
}
