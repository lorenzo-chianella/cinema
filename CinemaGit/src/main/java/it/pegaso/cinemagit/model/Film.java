package it.pegaso.cinemagit.model;

import java.sql.Time;

public class Film {

	private Long id;
	private String titolo;
	private String regista;
	private String genere;
	private Integer nPosti;
	private Time oraInizio;
	private String descrizione;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitolo() {
		return titolo;
	}
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	public String getRegista() {
		return regista;
	}
	public void setRegista(String regista) {
		this.regista = regista;
	}
	public String getGenere() {
		return genere;
	}
	public void setGenere(String genere) {
		this.genere = genere;
	}
	public Integer getnPosti() {
		return nPosti;
	}
	public void setnPosti(Integer nPosti) {
		this.nPosti = nPosti;
	}
	public Time getOraInizio() {
		return oraInizio;
	}
	public void setOraInizio(Time oraInizio) {
		this.oraInizio = oraInizio;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Titolo: " + getTitolo() + " | ");
		sb.append("Regista: " + getRegista() + " | ");
		sb.append("Ora di inizio: " + getOraInizio());
		return sb.toString();
	}
	
	
}
