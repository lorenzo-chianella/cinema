<%@page import="it.pegaso.cinemagit.model.Film"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Info sul film</title>
</head>
<body style="background-color: darkblue; color: white">
<%
 Film movie = (Film) request.getAttribute("film");
%>
<h1 style="text-align: center;"><%=movie.getTitolo() %></h1>
<h3>Sinossi:</h3>
<p><%=movie.getDescrizione() %></p>
<h3>Orario: </h3>
<p><%=movie.getOraInizio() %></p>

<a href="AcquistaBiglietti?orario=<%=movie.getOraInizio()%>&posti=<%=movie.getnPosti()%>"><input type="button" value="Acquista biglietti"></a>
</body>
</html>