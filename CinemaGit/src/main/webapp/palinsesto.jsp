<%@page import="it.pegaso.cinemagit.model.Film"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Film</title>
</head>
<body
	style="background-color: darkblue; text-align: center; color: white">
	<%
	List<Film> movies = (List<Film>) request.getAttribute("list");
	%>
	<%
	if (movies.size() > 0) {
	%>
	<h1>Oggi in sala</h1>
	<table align="center">
		<thead>
			<tr>
				<th style="width: 20em; border: solid white">Titolo</th>
				<th style="width: 20em; border: solid white">Regista</th>
				<th style="width: 20em; border: solid white">Genere</th>
			</tr>
		</thead>
		<tbody>
			<%
			for (Film f : movies) {
			%>
			<tr>
				<td style="width: 20em; border: solid white"><a
					href="GetMovieInfo?titolo=<%=f.getTitolo()%>" style="color: white"><%=f.getTitolo()%>
				</a></td>
				<td style="width: 20em; border: solid white"><%=f.getRegista()%></td>
				<td style="width: 20em; border: solid white"><%=f.getGenere()%></td>
			</tr>
			<%
			}
			%>
		</tbody>
	</table>

	<%
	} else {
	%>
	<h1>Non ci sono film in sala oggi</h1>
	<%
	}
	%>
</body>
</html>